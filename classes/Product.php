<?php
class Product{
    public $id;
    public $title;
    public $price;
    public $discription;
    public $type;

    public function __construct($id=NULL,$title,$price,$discription,$type){
        $this -> id = $id;
        $this -> title = $title;
        $this -> price = $price;
        $this -> description = $discription;
        $this -> type =$type;
    }
    
    public function save(PDO $db){
        try{
            $sql="INSERT INTO products SET
              title=:title,
              price=:price,
              description=:description,
              type=:type
            ";
            $statement=$db->prepare($sql);
            $statement->bindValue(':title', $this-> title);
            $statement->bindValue(':price', $this-> price);
            $statement->bindValue(':description', $this-> description);
            $statement->bindValue(':type', $this-> type);
            $statement->execute();
            
        }catch(Exception $e){
            die('Error inserting <br>'. $e->getMessage());
        } 
    }

    public function update($db){
        try{
            $sql = "UPDATE products SET
                title = :title,
                price = :price,
                description = :description,
                type = :type
                WHERE id=:id;
            ";
            $statement = $db->prepare($sql);
            
            $statement->bindValue(':title', $this-> title);
            $statement->bindValue(':price', $this-> price);
            $statement->bindValue(':description', $this-> description);
            $statement->bindValue(':type', $this-> type);
            $statement->bindValue(':id', $this-> id);
            $statement->execute();
        }catch(Exception $e){
            die('Error updating entry.' .$e->getMessage());
        }
    }
}
?>