<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/database/base.php';

try{
    $id = (int) $_GET['id'];
    $sql = "SELECT * FROM products WHERE id=:id";
    $statement = $db->prepare($sql);
    $statement->bindValue(':id', $id);
    $statement->execute();
    $data = $statement->fetchAll();
}catch (Exception $e) {
    die('Error getting entry.<br>' . $e->getMessage());
}
if(empty($data)){
    header('Location:/');
}
  $good = $data[0];
?>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/templates/header_connect.php';?>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/templates/header.php';?>
<div class="container">
    <div class="text-center">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title"><?=$good['title']?></h5>
                <p class="card-title"><?=$good['type']?></p>
                <p class="card-title"><?=$good['price']?></p>
                <p class="card-title"><?=$good['description']?></p>                
            </div>
        </div>    
    </div>
</div>    
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/templates/footer.php';?>