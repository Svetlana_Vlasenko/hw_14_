<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/database/base.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/Product.php';

$id = (int) $_POST['id'];
$title = htmlspecialchars($_POST['title'], ENT_QUOTES, 'UTF-8');
$price = htmlspecialchars($_POST['price'], ENT_QUOTES, 'UTF-8');
$description = htmlspecialchars($_POST['description'], ENT_QUOTES, 'UTF-8');
$type = htmlspecialchars($_POST['type'], ENT_QUOTES, 'UTF-8');

$product = new Product($id,$title,$price,$description,$type);
$product->update($db);

header('Location:/?message=entry_updated');
?>