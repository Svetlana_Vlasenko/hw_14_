<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/database/base.php';
try{
  $id = (int) $_GET['id'];
  $sql = "SELECT * FROM products WHERE id=:id";
  $statement = $db->prepare($sql);
  $statement->bindValue(':id', $id);
  $statement->execute();
  $data =$statement->fetchAll();
} catch (Exception $e) {
  die('Error getting entry.<br>' . $e->getMessage());
}
if(empty($data)){
  header('Location:/');
}
$good = $data[0];
?>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/templates/header_connect.php';?>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/templates/header.php';?>
<div class="container">
  <div class="body row justify-content-center ">
    <div class="col-5">
    <h1>Edit entry</h1>
      <form action="/product/update.php" method="post">
        <input type="hidden" name="id" value="<?= $id ?>">
        <div class="mb-3">
          <label class="form-label" for="title">Entry title:</label>
          <input class="form-control" name="title" type="text" id="title" value="<?=$good['title']?>">
        </div>
        <div class="mb-3">
          <label class="form-label" for="price">Entry price:</label>
          <input class="form-control" name="price" type="text" id="price" value="<?=$good['price']?>"> 
        </div>
        <div class="mb-3">
          <label class="form-label" for="description">Entry description:</label>
          <textarea class="form-control" name="description" type="text" id="description"><?=$good['description']?></textarea>
        </div>
        <div class="mb-3">
          <label class="form-label" for="type">Entry type:</label>
          <input class="form-control" name="type" type="text" id="type" value="<?=$good['type']?>">
        </div>
        <div class="mb-3">
         <button class="btn btn-primary">Edit entry</button>
        </div>
      </form>
    </div>
  </div>
</div>   
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/templates/footer.php';?>    