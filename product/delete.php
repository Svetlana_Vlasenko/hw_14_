<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/database/base.php';

$id = (int) $_GET['id'];
$sql = "DELETE FROM products WHERE id=:id";
try{
    $statement = $db->prepare($sql);
    $statement->bindValue(':id', $id);
    $statement->execute();
}catch(Exception $e){
    die('Error updating entry.' .$e->getMessage());
}
header('Location:/');

?>