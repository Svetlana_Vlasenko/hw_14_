<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/database/base.php';
// создание таблицы в БД
try{
    $sql = "CREATE TABLE products (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        title VARCHAR(255),
        price INT,
        description TEXT,
        type VARCHAR(255) )
        DEFAULT CHARACTER SET utf8 ENGINE = InnoDB ; ";
    $db->exec($sql);    
}catch(Exception $e){
    die('Error creating products  <br>'. $e->getMessage());
}
die('Products table creation successfully');
?>