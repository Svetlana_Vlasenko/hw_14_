<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/database/base.php';
//запрос на получение данных из таблицы
try{
  $sql="SELECT id,title,price,description,type FROM products;";
  $statmentObject = $db->query($sql);
  $goods = $statmentObject ->fetchAll();
}catch(Exception $e){
    die('Problem with getting data<br>'.$e->getMessage());
}
$message = NULL;
if (!empty($_GET['message'])) {
    $message = $_GET['message'];
} 
?>

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/templates/header_connect.php';?>
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/templates/header.php';?>
  
<div class="container">
    <div class="row">
        <?php if ($message) : ?>           
            <div class="alert alert-primary">
                Entry updated successfully!
            </div>
        <?php endif ?>
        <!-- вывод всех записей -->
        <?php foreach($goods as  $good):?>
            <div class=" col-4">
                <div class="card" style="height: 225px;">
                    <div class="card-body">
                        <h5 class="card-title"><?=$good['title']?></h5>
                        <p class="card-title"><?=$good['price']?></p>
                        <a href="/product/show.php?id=<?=$good['id']?>">More details</a>
                        <br>
                        <a href="/product/edit.php?id=<?=$good['id']?>">Edit entry</a>
                        <form action="/product/delete.php?id=<?= $good['id'] ?>" method="post"><button class="btn btn-link">Delete</button></form> 
                    </div>
                </div>    
            </div>
        <?php endforeach;?>
    </div>
</div>     

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/templates/footer.php';?>